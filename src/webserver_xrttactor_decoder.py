#!/usr/bin/python
import sys, re, base64, urllib
from optparse import OptionParser

OUTPUTFILE = 'output'										  #Ouput file name

def write_in_file(str_, outputfile):
	# Open a file in write mode
	fo = open(outputfile, "w+")

	# Write a line at the end of the file.
	fo.seek(0, 2)
	line = fo.write(str_)

	# Close opend file
	fo.close()
	print "Result stored successfully in ", fo.name


def parse_log(filename, begin, end):
	str_  = ''
	date  = ''
	regexp      = '(?<=' + begin + ').*?(?=' + end + ')'	  #(?<=#START#).*  start matching all characters once the first instance of #START# is passed, .*?(?=#END#) -> match all characters in a string up to, but not including the characters #END#
   	searchObj   = re.compile(regexp, re.S)				 	  #(?<=#START#).*?(?=#END#)
	line    	= ''
	request 	= ''
	lines   	= tuple(open(filename, 'r+'))       			  #Get tall lines within an array

	for index in range(len(lines)):
		line = lines[index]
		if len(line) > 0:									  #make sure we match a valid line and filled one (not an empty line)
			date           = line.split()[3]                  #Get the time it was passed
			date 		   = date[1:]						  #Remove the '[' in h ebegenning
			extracted_line = searchObj.findall(line)		  #call Re modul findall function to get the encoded base64 
			if len(extracted_line) > 0:
				ex   = extracted_line[0]
				ex   = urllib.unquote(urllib.unquote(ex))	  #decode the encoded UTF8 URL
				ex   = base64.decodestring(ex)
				str_ = str_ + date + '\n' + ex + "\n\n"
	return str_;

def main(argv):
	#get the arguments
	filename   	= ''
	outputfile  = ''
	begin		= ''
	end 		= ''

	parser = OptionParser("usage: %prog [options] Parse_filename", version="%prog 1.0")

	parser.add_option("-o", "--ofile",
		dest="ofile",
		metavar='<FILE NAME>',
		default=OUTPUTFILE,
		help="Specify the file to write the outputs, create if no existing")

	parser.add_option("-b", "--begin",
		dest="begin",
		metavar='<SUBSTRING>',
		default="",
		help="begin substring for the first condition (?<=#START#)")

	parser.add_option("-e", "--end",
		dest="end",
		metavar='<SUBSTRING>',
		default="",
		help="end   substring for the second condition (?=#END#)")

	(opts, args) = parser.parse_args()

	if len(args) != 1:
		parser.print_help()

	if opts.begin and not opts.end:
		print "OPTIONS BEGIN & END are required\n"
		parser.print_help()
		exit(-1)

	str_ = parse_log(args[0], opts.begin, opts.end)

	write_in_file(str_, opts.ofile)

	print "By Punkachu !\n"

if __name__ == "__main__":
   main(sys.argv[1:])