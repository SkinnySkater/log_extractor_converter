import sys, re, base64, urllib


frontnback_value = 0

def write_in_file(str_):
	# Open a file in write mode
	fo = open('output', "rw+")

	# Write a line at the end of the file.
	fo.seek(0, 2)
	line = fo.write(str_)

	# Close opend file
	fo.close()
	print "Result stored successfully in ", fo.name


def main(argv):
	#get the arguments
	str_       	= ''
	date		= ''
	filename   	= argv[0]
   	regexp      = '(?<=' + argv[1] + ').*?(?=' + argv[2] + ')'#(?<=#START#).*  start matching all characters once the first instance of #START# is passed, .*?(?=#END#) -> match all characters in a string up to, but not including the characters #END#
   	searchObj   = re.compile(regexp, re.S)				 	  #(?<=#START#).*?(?=#END#)

	line    	= ''
	request 	= ''
	lines   	= tuple(open(filename, 'r'))       			  # Get tall lines within an array

	for index in range(len(lines)):
		line = lines[index]
		if len(line) > 0:									  #make sure we match a valid line and filled one (not an empty line)
			date           = line.split()[3]                  #Get the time it was passed
			date 		   = date[1:]						  #Remove the '[' in h ebegenning
			extracted_line = searchObj.findall(line)		  #call Re modul findall function to get the encoded base64 
			if len(extracted_line) > 0:
				ex = extracted_line[0]
				ex = urllib.unquote(urllib.unquote(ex))		  #decode the encoded UTF8 URL
				ex = base64.decodestring(ex)
				str_ = str_ + date + '\n' + ex + "\n\n"
				
	print str_
	write_in_file(str_)
	print "By Punkachu !\n"

if __name__ == "__main__":
   main(sys.argv[1:])